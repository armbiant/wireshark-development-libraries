# Wireshark Development Libraries

This repository contains various libraries required to [build Wireshark](https://wiki.wireshark.org/Development/Support_library_version_tracking).
It currently contains Windows libraries, but libraries for other platforms and other assets might be added in the future.

It's currently published using GitLab Pages and is exposed to the internet at https://dev-libs.wireshark.org/.

Many of the packages were built using https://gitlab.com/wireshark/wireshark-vcpkg-scripts.